#/usr/bin/env bash
set -e

gradle -Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=7890 -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=7890 jar
cp -v ./build/libs/khala-0.0.1.jar $HOME/OneDrive/agora/ap_resource_info

