package org.khala.cli

import com.andreapivetta.kolor.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.multiple
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.choice
import me.tongfei.progressbar.ProgressBar
import me.tongfei.progressbar.ProgressBarBuilder
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.khala.features.ap.*
import org.khala.resource.*
import java.io.File
import java.io.FileOutputStream
import java.io.PrintWriter

class Resource: CliktCommand(name = "gen", help = "AccessPoint Config Generation") {
  private val operation by option("-o", "--op", help = "Operate to be executed. Default is BOTH.")
      .choice("UPDATE", "INSERT").default("BOTH")
  private val idc by option("-i", "--idc", help = "Specify idc generate. Default is ALL.").multiple()
  private val target by option("-t", "--target", help = "Target backend-service. Default is ALL.")
      .choice(*ServiceNames.keys.toTypedArray()).multiple()
  private val verbose by option("-v", "--verbose",
      help = "Enable low-level detail messages.").flag(default = false)
  private val alwaysYes by option("-y", "--yes",
      help = "Always say yes for all confirmation. Disable by default.").flag(default = false)
  private val noDoubleCheck by option("--no-double-check",
      help = "Disable double-check step.").flag(default = false)
  private val progress by option("--progress",
      help = "Display progress bar").flag(default = false)
  private val debug by option("--debug",
      help = "Enter Debug Mode. Only generate sql statement, but not execute. "
          + "This will enable doubleCheck and alwaysYes both.").flag(default = false)
  private val dump by option("--dump", help = "Dump sql instead of running it.").flag(default = false)
  private val outputFileName by option("--output",
      help = "Sql filename. This option is only available"
          + " when 'dump' is enabled.").default("output.sql")
  private var fp: PrintWriter? = null

  private fun continueAsk(): Boolean {
    if (alwaysYes || debug) return true
    print("Do you want to continue [y/N]? ")
    readLine()?.let {
      val msg = it.trim()
      if (msg == "y" || msg == "yes") {
        return true
      }
      return false
    }
    return false
  }

  private fun doubleCheckUpdation(updations: List<ConfigUpdation>): Boolean {
    if (debug || !noDoubleCheck) {
      println("Updations Double-Check, following updations will take effect:")
      println("Following updations will take effect:")
      updations.map {
        println("____updation for idc: "+ it.idc.cyan() + ", service_name: "
            + it.serviceName.cyan())
        it.toStringBuffer().map { line ->
          println("  $line")
        }
      }
      println()
    }

    return continueAsk()
  }

  private fun doubleCheckInsertion(insertion: List<ConfigInsertion>): Boolean {
    if (!noDoubleCheck) {
      println("Insertion Double-Check, following insertions will take effect:")
      println("Following insertions will take effect:")
      insertion.map {
        println("____insertion for idc: "+ it.idc.cyan() + ", service_name: "
            + it.serviceName.cyan())
        it.toStringBuffer().map { line ->
          println("  $line")
        }
      }

      println()
    }

    return continueAsk()
  }

  private fun operationUpdate(name: String) {
    val updations = makeUpdations(name, progress, idc)
    if (updations != null) {
      if (updations.isEmpty()) {
        println("There's no outdated config for service ".yellow() + name.black().lightYellowBackground())
      } else {
        if (doubleCheckUpdation(updations)) {
          /*
          var pb: ProgressBar? = null
          if (progress) {
            pb = ProgressBarBuilder().setInitialMax(updations.size.toLong()).setTaskName("EXEC UPDATE").build()
          }

          pb.use { _ ->
            updations.forEach { updation ->
              pb?.extraMessage = updation.idc

              transaction(agoraResource) {
                val stmt = IdcConfigTable.updateQuery({
                  IdcConfigTable.idc.eq(updation.idc) and IdcConfigTable.serviceName.eq(updation.serviceName) }) {
                  with (SqlExpressionBuilder) {
                    it.update(IdcConfigTable.version, IdcConfigTable.version + 1)
                  }
                  it[IdcConfigTable.config] = updation.newData
                }

                if (debug || dump) {
                  toPlainSQL(stmt, this).forEach {
                    if (debug) {
                      println("____execute sql statement for [ idc: ".green() + updation.idc.cyan()
                          + ", service_name: ".green() + updation.serviceName.cyan() + " ]".green())
                      println(it)
                    }
                    if (dump) {
                      fp!!.writeText("$it;\n")
                    }
                  }
                } else {
                  this.commit()
                }
              }

              pb?.step()
            }
          }
        }
        */
          if (verbose || dump) {
            updations.forEach { updation ->
              transaction(agoraResource) {
                val stmt = IdcConfigTable.updateQuery({
                  IdcConfigTable.idc.eq(updation.idc) and IdcConfigTable.serviceName.eq(updation.serviceName) }) {
                  with (SqlExpressionBuilder) {
                    it.update(IdcConfigTable.version, IdcConfigTable.version + 1)
                  }
                  it[IdcConfigTable.config] = updation.newData
                }

                toPlainSQL(stmt, this).forEach {
                  if (verbose) {
                    println("____execute sql statement for [ idc: ".green() + updation.idc.cyan()
                        + ", service_name: ".green() + updation.serviceName.cyan() + " ]".green())
                    println(it)
                  }
                  if (dump) {
                    fp?.println("$it;")
                  }
                }
              }
            }
          }

          if (!debug && !dump) {
            var pb: ProgressBar? = null
            if (progress) {
              pb = ProgressBarBuilder().setInitialMax(updations.size.toLong()).setTaskName("EXEC UPDATE").build()
            }

            pb.use { _ ->
              transaction(agoraResource) {
                updations.forEach { updation ->
                  pb?.extraMessage = updation.idc

                  IdcConfigTable.update({
                    IdcConfigTable.idc.eq(updation.idc) and IdcConfigTable.serviceName.eq(updation.serviceName) }) {
                    with (SqlExpressionBuilder) {
                      it.update(IdcConfigTable.version, IdcConfigTable.version + 1)
                    }
                    it[IdcConfigTable.config] = updation.newData
                  }

                  pb?.step()
                }

                this.commit()
              }
            }
          }
        }
      }
    }
  }

  private fun operationInsert(name: String) {
    var insertions = makeInsertion(name, progress, idc)
    if (insertions != null) {
      if (insertions.isEmpty()) {
        println("____there's no missing config for service ".yellow() + name.cyan())
      } else {
        if (doubleCheckInsertion(insertions)) {
          if (verbose || dump) {
            insertions.forEach { insertion ->
              transaction(agoraResource) {
                val stmt = IdcConfigTable.insertQuery {
                  it[idc] = insertion.idc
                  it[serviceName] = insertion.serviceName
                  it[config] = insertion.newData
                  it[version] = 1
                }

                toPlainSQL(stmt, this).forEach {
                  if (verbose) {
                    println("____execute sql statement for [ idc: ".green() + insertion.idc.cyan()
                        + ", service_name: ".green() + insertion.serviceName.cyan() + " ]".green())
                    println(it)
                  }
                  if (dump) {
                    fp?.println("$it;")
                  }
                }
              }
            }
          }

          if (!debug && !dump) {
            var pb: ProgressBar? = null
            if (progress) {
              pb = ProgressBarBuilder().setInitialMax(insertions.size.toLong()).setTaskName("EXEC INSERT").build()
            }

            pb.use { _ ->
              transaction(agoraResource) {
                insertions.forEach {  insertion ->
                  pb?.step()
                  pb?.extraMessage = insertion.idc

                  IdcConfigTable.insert {
                    it[idc] = insertion.idc
                    it[serviceName] = insertion.serviceName
                    it[config] = insertion.newData
                    it[version] = 1
                  }
                }

                this.commit()
              }
            }
          }
        }
      }
    }
  }

  override fun run() {
    var tgt = target
    if (tgt.isEmpty()) {
      tgt = ServiceNames.keys.toList()
    }


    println("Generation Info:")
    println("  idc: " + "[${idc.joinToString()}]".green())
    println("  target: " + tgt.toString().green())
    println("  verbose: " + verbose.toString().green())
    println("  always yes: " + alwaysYes.toString().green())
    println("  no double check: " + noDoubleCheck.toString().green())
    println("  op: " + operation.green())
    println("  debug mode: " + debug.toString().green())
    println("  dump mode: " + dump.toString().green())
    println("  output filename: " + outputFileName.green())
    println("===========================================================")

    if (dump) {
      fp = PrintWriter(outputFileName)
    }

    tgt.forEach { name ->
      println("Generate config for $name...")

      if (operation == "UPDATE" || operation == "BOTH") {
        operationUpdate(name)
      }

      if (operation == "INSERT" || operation == "BOTH") {
        operationInsert(name)
      }
    }

    fp?.flush()
  }
}
