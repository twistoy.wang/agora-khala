package org.khala

import org.jetbrains.exposed.sql.ResultRow
import org.khala.resource.ServerTable

data class ServerIp(val isp: String, val ip: String) {
  fun toDebugString(): String {
    return "$isp:$ip"
  }
}

class Server(val name: String, val detailIp: List<ServerIp>, val idc: String) {
  companion object {
    val NormalISP = arrayOf("CUCC", "CMCC", "CTEL", "BGP", "LGP", "3LINE")
    val knownISP = arrayOf("CUCC", "CMCC", "CTEL", "BGP", "LGP", "3LINE", "NA")
    val ispOrderAboard = arrayOf("CUCC", "CTEL", "CMCC", "BGP", "LGP")
    val ispOrder3Line = arrayOf("CTEL", "CUCC", "CMCC", "BGP", "LGP")
    var bgpCities = arrayOf("shanghai", "guangzhou", "beijing", "shenzhen",
        "hangzhou", "huabei", "qingdao")
  }

  override fun toString(): String {
    return "[Server name = $name, idc = $idc, detail_ip = ${detailIp()}]"
  }

  fun detailIp(): String {
    return detailIp.joinToString(";") { svr -> svr.toDebugString() }
  }

  fun findISP(ip: String): String? {
    val found = detailIp.find { serverIp -> serverIp.ip == ip }
    if (found != null) return found.isp
    return null
  }

  private fun isNA(): Boolean {
    return detailIp.size == 1 && detailIp[0].isp == "NA"
  }

  fun getIsp(): String {
    if (isNA()) {
      return "NA"
    }
    if (detailIp.size > 1) {
      return "3LINE"
    }
    return detailIp[0].isp
  }

  fun select(reqIsp: String): String? {
    val isp = reqIsp.toUpperCase()
    if (!isKnownISP(isp)) {
      return null
    }

    if (isNA()) {
      return detailIp[0].ip
    }

    detailIp.find { serverIp -> serverIp.isp == isp }?.let {
      return it.ip
    }

    if (isp == "NA") {
      ispOrderAboard.map { i -> detailIp.find { sip -> sip.isp == i } }.first { ip -> ip != null }?.let {
        return it.ip
      }
    } else {  // request isp is 3LINE or BGP
      ispOrder3Line.map { i -> detailIp.find{ sip -> sip.isp == i } }.first { ip -> ip != null }?.let {
        return it.ip
      }
    }

    return null
  }
}

fun isKnownISP(queryISP: String): Boolean {
  return Server.knownISP.any { isp -> isp == queryISP }
}

private fun makeServerIp(ipString: String): ServerIp {
  val arr = ipString.trim().split(":")
  return ServerIp(arr[0].trim(), arr[1].trim())
}

fun makeServer(row: ResultRow): Server {
  val name = row[ServerTable.name]
  val detailIp = row[ServerTable.detailIp]
  val idc = row[ServerTable.idc]
  return Server(name,
      detailIp.trim().split(";").map { ipString -> makeServerIp(ipString) }.toList(),
      idc)
}

fun idc2ISP(idc: String): String {
  val uidc = idc.toUpperCase()
  Server.NormalISP.firstOrNull {
    uidc.contains(it)
  }?.let {
    return it
  }

  if (idc == "jiaxing-2" || idc == "wuxi-1") {
    return "3LINE"
  }

  val lidc = idc.toLowerCase()
  Server.bgpCities.firstOrNull{
    lidc.contains(it)
  }?.let {
    return "BGP"
  }

  return "NA"
}
