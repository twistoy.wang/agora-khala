package org.khala.resource

import org.jetbrains.exposed.sql.*
import org.khala.getGlobalSettings

object VendorsTable: Table("vendor_info") {
  val vendorId: Column<Int> = integer("vendor_id")
  val key: Column<String> = varchar("key", 32)
  val maxSubscribeLoad: Column<Int> = integer("max_subscribe_load")
  val maxResolution: Column<String> = varchar("max_resolution", 100)
  val status: Column<Int> = integer("status")
  val region: Column<Int> = integer("region")
}

val miniappDatabase = Database.connect(
  "jdbc:mysql://${getGlobalSettings().miniapp.ip}:${getGlobalSettings().miniapp.port}/${getGlobalSettings().miniapp.dbName}",
  driver = "com.mysql.jdbc.Driver", user = getGlobalSettings().miniapp.userName,
  password = getGlobalSettings().miniapp.password)
