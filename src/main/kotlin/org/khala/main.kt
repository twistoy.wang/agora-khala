package org.khala

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import org.khala.cli.Miniapp
import org.khala.cli.Resource

val VERSION = "0.0.1"

class KhalaCommand: CliktCommand() {
  private val version by option("-v", "--version", help = "Show version message.").flag(default = false)
  override fun run() {
    if (version) {
      println(VERSION)
      return
    }
  }
}


fun main(args: Array<String>) = (KhalaCommand().subcommands(Resource(), Miniapp())).main(args)

