package org.khala.features.ap

import com.andreapivetta.kolor.blue
import com.andreapivetta.kolor.green
import com.andreapivetta.kolor.red
import com.andreapivetta.kolor.yellow
import com.google.gson.Gson
import com.google.gson.JsonElement
import me.tongfei.progressbar.ProgressBar
import me.tongfei.progressbar.ProgressBarBuilder
import org.khala.idc2ISP

val ServiceNames: Map<String, String> = hashMapOf(
    "whiteList" to "/ap_ip_white_list",
    "cdn" to "/cdn_dispatcher_server",
    "cds" to "/cds_server",
    "p2p" to "/p2p_lbs_server_v2",
    "report" to "/report_lbs_server_v2",
    "sig0" to "/sig_env0_lbs_server_v2",
    "sig1" to "/sig_env1_lbs_server_v2",
    "tds" to "/tds_server",
    "appCenter" to "/uap_app_center_server",
    "voice" to "/voice_lbs_server_v2",
    "webrtc" to "/webrtc_lbs_server_v2",
    "webrtc2" to "/webrtc2_lbs_server_v2",
    "voet" to "/voet_lbs_server_v2",
    "sua" to "/sua_server",
    "flv" to "/flv_lbs_server_v2",
    "imgPrediction" to "/img_prediction_lbs_server_v2"
)

data class BackendServiceBinding(val address: String, val tags: Set<String>) {
  companion object {
    fun fromJson(json: JsonElement): BackendServiceBinding {
      if (json.isJsonObject) {
        return Gson().fromJson<BackendServiceBinding>(json, BackendServiceBinding::class.java)
      }

      return BackendServiceBinding(json.asString, emptySet())
    }
  }

  fun toJson(): String {
    if (tags.isEmpty()) {
      return Gson().toJson(address)
    }
    return Gson().toJson(this)
  }
}

data class BackendServiceServer(val address: String, val port: Int?, val tags: Set<String>?)
data class BackendService(val name: String, val port: Int, val simple: Boolean = false, val servers: List<BackendServiceServer>) {
  fun toJson(isp: String): String = Gson().toJson(servers.map {
    if (simple) {
      it.address
    } else {
      val server = it
      val pt = it.port ?: this.port
      if (it.tags != null) {
        if (server.address == "127.0.0.1") {
          BackendServiceBinding("127.0.0.1:$pt", it.tags)
        } else {
          BackendServiceBinding("${findServerByIp(server.address)?.select(isp)}:$pt", it.tags)
        }
      } else {
        if (server.address == "127.0.0.1") {
          "127.0.0.1:$pt"
        } else {
          "${findServerByIp(server.address)?.select(isp)}:$pt"
        }
      }
    }
  })
}

data class ServiceListDifference(var newSet: List<BackendServiceBinding>,
                                 var removeSet: List<BackendServiceBinding>,
                                 var modifySet: List<Pair<BackendServiceBinding, BackendServiceBinding>>)
data class ConfigUpdation(val idc: String, val serviceName: String, val oldData: String,
                          val newData: String, val oldVersion: Int, val diff: ServiceListDifference) {
  fun toStringBuffer(): List<String> {
    var lines = emptyList<String>()
    lines += "__configuration updation:".yellow()
    diff.removeSet.map {
      lines += "  - $it".red()
    }
    diff.newSet.map {
      lines += "  + $it".green()
    }
    diff.modifySet.map {
      lines += "  * $it".blue()
    }

    return lines
  }
}
data class ConfigInsertion(val idc: String, val serviceName: String, val newData: String) {
  fun toStringBuffer(): List<String> {
    var lines = emptyList<String>()

    lines += "____configuration insertion:".yellow()
    parseConfigBody(newData).map {
      lines += "  + $it".green()
    }

    return lines
  }
}

fun makeUpdations(name: String, progress: Boolean, requestIdc: List<String>): List<ConfigUpdation>? {
  val serviceName = ServiceNames[name] ?: return null
  val allAccessPointIdc = filterAccessPointIdc()
  var pb: ProgressBar? = null

  var idcSet = allAccessPointIdc.toSet()
  if (requestIdc.isNotEmpty()) {
    idcSet = requestIdc.toSet()
  }

  if (progress) {
    pb = ProgressBarBuilder().setInitialMax(idcSet.size.toLong()).setTaskName("UPDATE").build()
  }

  pb.use {
    return filterAccessPointIdc().mapNotNull idcFilter@{ idc ->
      if (!idcSet.contains(idc)) {
        return@idcFilter null
      }
      pb?.extraMessage = idc

      val oldConfigData = fetchIdcConfig(idc, serviceName)
      if (oldConfigData == null) {
        pb?.step()
        return@idcFilter null
      }

      pb?.step()

      val newConfigData = readBackendServiceConfigs(name).toJson(idc2ISP(idc))

      if (eqBackendServicesBindingList(parseConfigBody(oldConfigData.config), parseConfigBody(newConfigData))) {
        return@idcFilter null
      }

      ConfigUpdation(idc, oldConfigData.serviceName,
          oldConfigData.config, newConfigData, oldConfigData.version, checkBackendServiceListDifferences(
          parseConfigBody(oldConfigData.config), parseConfigBody(newConfigData)
      ))
    }.toList()
  }
}

fun makeInsertion(name: String, progress: Boolean, requestIdc: List<String>): List<ConfigInsertion>? {
  val serviceName = ServiceNames[name] ?: return null
  val allAccessPointIdc = filterAccessPointIdc()
  var pb: ProgressBar? = null

  var idcSet = allAccessPointIdc.toSet()
  if (requestIdc.isNotEmpty()) {
    idcSet = requestIdc.toSet()
  }

  if (progress) {
    pb = ProgressBarBuilder().setInitialMax(idcSet.size.toLong()).setTaskName("UPDATE").build()
  }

  pb.use {
    return allAccessPointIdc.mapNotNull idcFilter@{ idc ->
      if (!idcSet.contains(idc)) {
        return@idcFilter null
      }

      pb?.step()
      pb?.extraMessage = idc

      fetchIdcConfig(idc, serviceName)?.let {
        return@idcFilter null
      }

      ConfigInsertion(idc, serviceName, readBackendServiceConfigs(name).toJson(idc2ISP(idc)))
    }
  }
}
